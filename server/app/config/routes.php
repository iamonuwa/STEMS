<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'main';

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

$route['about-us']				= 'pages/about';

$route['contact-us']			= 'pages/contact';

$route['login']					= 'main/login';

$route['logout'] 				= 'backend/admin/logout'; 

$route['admin/dashboard'] 		= 'backend/admin/index';

$route['officer/dashboard'] 	= 'backend/admin/index';

$route['api/v1']				= 'backend/api/api/index';

$route['api/example/users/(:num)'] = 'api/example/users/id/$1';

$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4';