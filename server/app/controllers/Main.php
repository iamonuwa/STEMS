<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Frontend_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Aauth');
	}
	
	public function index()
	{
		if($this->aauth->is_loggedin()){
			if($this->aauth->is_admin()){
			redirect(base_url('admin/dashboard'),'refresh');
		}
		else{
			redirect(base_url('officer/dashboard'),'refresh');
			}
	}else{
		$this->title = "Admin CPanel";
		$this->load->view('layout/header');
		$this->load->view('auth/login');
		$this->load->view('layout/footer');
		//$this->load->view('themes/'.$this->site->get_theme()->theme_name.'/login');
		}
	}

	public function login()
	{
		$this->email = html_escape($this->input->post('email'));
		$this->password = html_escape($this->input->post('password'));
		$this->rememberCode = $this->input->post('remember');
		$check = $this->authLogin($this->email, $this->password, $this->rememberCode);
		if($check){
			$this->session->set_flashdata('success', '');
			redirect(base_url('admin/dashboard'),'refresh');
		}
			$this->session->set_flashdata('msg', $this->aauth->print_errors());
			redirect(base_url(),'refresh');
		
	}
	public function forgot()
	{
		$this->email = html_escape($this->input->post('email'));
		if($this->sendReset($this->email)){
			$this->session->set_flashdata('success', '');
			redirect(base_url('admin/dashboard'),'refresh');
		}
		$this->session->set_flashdata('msg', $this->aauth->print_errors());
		redirect(base_url(),'refresh');
	}
	
}
