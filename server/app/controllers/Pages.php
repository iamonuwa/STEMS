<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function about()
	{
		$this->title = "About Us";
		$this->load->view('layout/header');
		$this->load->view('pages/about');
		$this->load->view('layout/footer');
	}

		public function contact()
	{
		$this->title = 'Contact Us';
		$this->load->view('layout/header');
		$this->load->view('pages/contact');
		$this->load->view('layout/footer');
	}

}

/* End of file Pages.php */
/* Location: .//opt/lampp/htdocs/stems/server/app/controllers/Pages.php */