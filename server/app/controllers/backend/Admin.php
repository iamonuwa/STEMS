<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->title = "Dashboard";
		$this->load->view('layout/header');
		$this->load->view('admin/dashboard');
		$this->load->view('layout/footer');
	}
}

/* End of file Admin.php */
/* Location: .//opt/lampp/htdocs/project/server/app/controllers/Backend/Admin.php */