<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Officer extends Backend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->title = "Dashboard";
		$this->load->view('layout/header');
		$this->load->view('officer/dashboard');
		$this->load->view('layout/footer');
	}

}

/* End of file Officer.php */
/* Location: .//opt/lampp/htdocs/stems/server/app/controllers/backend/Officer.php */