<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('site');
		$this->load->helper('url','form');
		//$this->_init();
	}

	private function _init()
	{
		$this->output->set_template($this->site->get_theme()->theme_name.'/'.$this->site->get_theme()->theme_name);
	}

	/*
	* Redirects Url
	*/
	public function redirect_url()
	{
	if( $this->session->userdata('redirect_back') ) {
   	$redirect_url = $this->session->userdata('redirect_back'); 
   	$this->session->unset_userdata('redirect_back');
   	redirect( $redirect_url );
   		}
	}

	/**
	* Login Script
	* Params
	* Requires Login Email Address => String
	* Requires Password => String
	* Requires RememberCode => Boolean
	**/

	public function authLogin($email, $password, $rememberCode)
	{
		return $this->aauth->login($email, $password , $rememberCode, $totp_code = NULL);;
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */