<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function logout()
	{
		if($this->aauth->logout()){
			redirect(base_url(),'refresh');
       	}
	}

}

/* End of file Backend_Controller.php */
/* Location: .//opt/lampp/htdocs/project/server/app/libraries/Backend_Controller.php */