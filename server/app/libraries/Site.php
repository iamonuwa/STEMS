<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Site {

	/**
	 * The CodeIgniter object variable
	 * @access public
	 * @var object
	 */
	public $CI;

	/**
	 * Variable for loading the config array into
	 * @access public
	 * @var array
	 */
	public $config_vars;

	/**
	 * Array to store error messages
	 * @access public
	 * @var array
	 */
	public $errors = array();

	/**
	 * Array to store info messages
	 * @access public
	 * @var array
	 */
	public $infos = array();
	
	/**
	 * Local temporary storage for current flash errors
	 *
	 * Used to update current flash data list since flash data is only available on the next page refresh
	 * @access public
	 * var array
	 */
	public $flash_errors = array();

	/**
	 * Local temporary storage for current flash infos
	 *
	 * Used to update current flash data list since flash data is only available on the next page refresh
	 * @access public
	 * var array
	 */
	public $flash_infos = array();

	/**
     * The CodeIgniter object variable
	 * @access public
     * @var object
     */
    public $site_db;

	########################
	# Base Functions
	########################

	/**
	 * Constructor
	 */
	public function __construct() {

		// get main CI object
		$this->CI = & get_instance();

		// Dependancies
		if(CI_VERSION >= 2.2){
			$this->CI->load->library('driver');
		}
		$this->CI->load->helper('url');
		$this->CI->load->helper('language');
		$this->CI->load->library('session');
		$this->CI->lang->load('site_lang');

 		// config/site.php
		$this->CI->config->load('site');
		$this->config_vars = $this->CI->config->item('site');
		//$this->site_db = $this->CI->load->database('pdo', true);
		$this->site_db = $this->CI->load->database($this->config_vars['app_db'], TRUE); 
		
		// load error and info messages from flashdata (but don't store back in flashdata)
		$this->errors = $this->CI->session->flashdata('errors');
		$this->infos = $this->CI->session->flashdata('infos');
	}

	/**
	 * Get Application Theme Based on Current Theme
	 * Get Application Theme Information
	 * @param int|bool $app_id Application ID to get or FALSE for current theme
	 * @return object Application Theme information
	 */
	public function get_theme() {

		$query = $this->site_db->where('is_current', $this->config_vars['is_current']);
		$query = $this->site_db->get($this->config_vars['theme_table']);

		if ($query->num_rows() <= 0){
			return FALSE;
		}
		return $query->row();
	}
	/**
	 * Get Application Status
	 * Get Application Information
	 * @return object Application information
	 */
	public function site_info() {

		$query = $this->site_db->where('app_status', $this->config_vars['app_state']);
		$query = $this->site_db->get($this->config_vars['app_table']);

		if ($query->num_rows() <= 0){
			return FALSE;
		}
		return $query->row();
	}
}
