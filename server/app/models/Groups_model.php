<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_model extends MY_Model {


	protected $_table = 'stems_aauth_to_group';

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
    protected $primary_key = 'id';
	

}

/* End of file Groups_model.php */
/* Location: .//opt/lampp/htdocs/stems/server/app/models/Groups_model.php */