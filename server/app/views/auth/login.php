<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section id="banner">
</section>
		<section id="main" class="container">

					<section id="login" class="box special features">
						<h3 >STEMS Login Controller</h3>
									<form method="post" method="POST" action="<?= base_url('main/login');?>" role="form" autocomplete="off">
										<div class="row uniform 50%">
											<div class="12u">
												<input type="text" ng-model="email" name="email" id="email" placeholder="Login Email Address" />
											</div>
											{{ email }}
											<div class="12u">
												<input type="password" name="password" id="password" placeholder="Login Password" />
											</div>
										</div>
										<div class="row uniform">
											<div class="12u">
												<ul class="actions">
													<li><input type="submit" value="Login" /></li>
												</ul>
											</div>
										</div>
									</form>

					</section>

				</section>