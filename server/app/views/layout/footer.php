<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; <?= date('Y');?> Onuwa Nnachi Isaac. All rights reserved.</li>
					</ul>
				</footer>

		</div>
			<script src="<?= base_url('assets/alpha/js/jquery.min.js');?>"></script>
			<script src="<?= base_url('assets/alpha/js/jquery.dropotron.min.js');?>"></script>
			<script src="<?= base_url('assets/alpha/js/jquery.scrollgress.min.js');?>"></script>
			<script src="<?= base_url('assets/alpha/js/skel.min.js');?>"></script>
			<script src="<?= base_url('assets/alpha/js/util.js');?>"></script>
			<!--[if lte IE 8]><script src="<?= base_url('assets/alpha/js/ie/respond.min.js');?>"></script><![endif]-->
			<script src="<?= base_url('assets/alpha/js/main.js');?>"></script>
			<script src="<?= base_url('assets/angular/angular.min.js');?>"></script>
			<script src="<?= base_url('assets/angular/angular-app.js');?>"></script>
			
	</body>
</html>