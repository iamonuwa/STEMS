<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title><?= $this->title.' - Timetable Management System';?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="<?= base_url('assets/alpha/js/ie/html5shiv.js');?>"></script><![endif]-->
		<link rel="stylesheet" href="<?= base_url('assets/alpha/css/main.css');?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="<?= base_url('assets/alpha/css/ie8.css');?>" /><![endif]-->
	<script>
      var BASE_URL = "<?php echo base_url(); ?>";
    </script>
	</head>
	<body class="landing" ng-app>
		<div id="page-wrapper">
				<header id="header" class="alt">
					<h1><a href="<?= base_url();?>">STEMS</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<?php if($this->aauth->is_loggedin()){?>
							<li>
								<a href="#" class="icon fa-angle-down"><?= $this->aauth->get_user()->fullname;?></a>
								<ul>
									<li><a href="generic.html">Generic</a></li>
									<li><a href="contact.html">Contact</a></li>
									<li><a href="<?= base_url('logout');?>">Log Me Out</a></li>
								</ul>
							</li>
							<?php }?>
						<li><a href="<?= base_url('about-us');?>">About Us</a></li>
						<li><a href="<?= base_url('contact-us');?>">Contact Us</a></li>
						</ul>
					</nav>
				</header>
